<?php
/**
 * rowstyle plugin for Craft CMS 3.x
 *
 * Row style field to reuse
 *
 * @link      ronaldvaneede.me
 * @copyright Copyright (c) 2018 Ronald van Eede
 */

/**
 * rowstyle en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('rowstyle', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Ronald van Eede
 * @package   Rowstyle
 * @since     0.0.1
 */
return [
    'rowstyle plugin loaded' => 'rowstyle plugin loaded',
];
