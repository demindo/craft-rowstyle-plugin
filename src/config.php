<?php
/**
 * rowstyle plugin for Craft CMS 3.x
 *
 * Row style field to reuse
 *
 * @link      ronaldvaneede.me
 * @copyright Copyright (c) 2018 Ronald van Eede
 */

/**
 * rowstyle config.php
 *
 * This file exists only as a template for the rowstyle settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'rowstyle.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "someAttribute" => true,

];
