/**
 * rowstyle plugin for Craft CMS
 *
 * rowstyle JS
 *
 * @author    Ronald van Eede
 * @copyright Copyright (c) 2018 Ronald van Eede
 * @link      ronaldvaneede.me
 * @package   Rowstyle
 * @since     0.0.1
 */
